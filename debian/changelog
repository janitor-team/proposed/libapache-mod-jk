libapache-mod-jk (1:1.2.48-1) unstable; urgency=medium

  * New upstream version 1.2.48.
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.
  * Use canonical VCS URI.
  * Suggest only the most recent version of tomcat.
  * Build-depend on default-jdk and ant to build the documentation.
  * Change the logic for building the documentation from source. Use ant.
  * Install the NOTICE file.
  * Drop 0001-disable-logo.patch and fix-privacy-breach.patch. Fixed upstream.

 -- Markus Koschany <apo@debian.org>  Thu, 04 Jun 2020 21:42:29 +0200

libapache-mod-jk (1:1.2.46-2) unstable; urgency=medium

  * Rename httpd-jk.conf to jk.conf to restore compatibility with Debian's
    Apache helpers a2enmod and a2dismod. (Closes: #928813)

 -- Markus Koschany <apo@debian.org>  Wed, 27 May 2020 19:19:20 +0200

libapache-mod-jk (1:1.2.46-1) unstable; urgency=medium

  * New upstream version 1.2.46.
  * Update debian/watch, import upstream signing key and verify tarballs.

 -- Markus Koschany <apo@debian.org>  Sun, 14 Oct 2018 12:26:05 +0200

libapache-mod-jk (1:1.2.44-3) unstable; urgency=medium

  * Remove conf/httpd-jk.conf from debian/clean to fix a FTBFS when building
    binary-arch target.

 -- Markus Koschany <apo@debian.org>  Sat, 06 Oct 2018 11:11:21 +0200

libapache-mod-jk (1:1.2.44-2) unstable; urgency=medium

  * Fix broken httpd-jk symlink.
    Thanks to Andreas Beckmann for the report. (Closes: #910160)

 -- Markus Koschany <apo@debian.org>  Wed, 03 Oct 2018 13:38:45 +0200

libapache-mod-jk (1:1.2.44-1) unstable; urgency=medium

  * New upstream version 1.2.44.
  * Declare compliance with Debian Policy 4.2.1.
  * Remove Damien Raude-Morvan from Uploaders. Add myself to Uploaders.
    (Closes: #889461)
  * Suggest alternative tomcat9 package.
  * Drop obsolete libapache2-mod-jk.NEWS.
  * Install new httpd-jk.conf file which follows Apache 2.4 syntax.
    (Closes: #786635)

 -- Markus Koschany <apo@debian.org>  Mon, 01 Oct 2018 19:15:34 +0200

libapache-mod-jk (1:1.2.43-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
  * Standards-Version updated to 4.1.3
  * Switch to debhelper level 11

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 12 Mar 2018 16:22:30 +0100

libapache-mod-jk (1:1.2.42-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.2.42.
  * Switch to compat level 10.
  * Remvove virtual package dh-apache2 from Build-Depends.
  * Declare compliance with Debian Policy 3.9.8.
  * Remove autotools-dev because we use compat 10 now.
  * Move the package to Git.

 -- Markus Koschany <apo@debian.org>  Sat, 08 Oct 2016 16:00:51 +0200

libapache-mod-jk (1:1.2.41-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 1.2.41.
  * Drop README.source. We use regular upstream releases again.
  * Update get-orig-source target. Use --verbose and --download-current-version
    flags.
  * Drop disable-libtool-check.patch. Not required for normal releases.
  * Vcs-Browser: Use https.
  * Remove autoconf and automake from Build-Depends again.
  * Run wrap-and-sort -sa.
  * Add clean file and ensure libapache-mod-jk can be built twice in a row.
  * debian/rules: Remove override for dh_auto_clean.
  * Update debian/copyright for new release.

 -- Markus Koschany <apo@debian.org>  Fri, 30 Oct 2015 22:33:34 +0100

libapache-mod-jk (1:1.2.40+svn150520-1) unstable; urgency=high

  * Team upload.
  * Imported Upstream SVN snapshot version 1.2.40+svn150520.
    - Fix CVE-2014-8111: (Closes: #783233)
      Apache Tomcat Connectors (mod_jk) ignored JkUnmount rules for subtrees of
      previous JkMount rules, which allows remote attackers to access otherwise
      restricted artifacts via unspecified vectors.
  * debian/control: Build-Depend on debhelper >= 9.
  * Remove source.lintian-overrides since we now build-depend on debhelper >=9.
  * Drop 0004-corrupted-worker-activation-status.patch. Fixed upstream.
  * debian/rules:
    - Disable sed command in debian/rules. Apparently not necessary for this
      release.
    - Run buildconf.sh before dh_auto_configure step since this is a requirement
      for building SVN snapshots.
    - Update dh_auto_clean override. Ensure that the package can be built twice
      in a row.
  * debian/control:
    - Add autoconf to Build-Depends.
    - Add automake to Build-Depends.
    - Remove Conflicts and Replaces fields because they are obsolete.
  * Add disable-libtool-check.patch and fix a FTBFS. We already build-depend on
    libtool but the script is not smart enough.
  * Add fix-privacy-breach.patch and fix lintian errors about "privacy breach
    logo".
  * Update debian/copyright information. Add missing BSD-3-clause license.
  * Add README.source.

 -- Markus Koschany <apo@gambaru.de>  Thu, 21 May 2015 17:53:24 +0200

libapache-mod-jk (1:1.2.37-4) unstable; urgency=medium

  * Team upload.
  * Switched to tomcat8 (Closes: #759624)
  * Standards-Version updated to 3.9.6 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 17 Nov 2014 14:52:23 +0100

libapache-mod-jk (1:1.2.37-3) unstable; urgency=low

  * d/rules: Fix "Hardening CPPFLAGS missing" (Closes: #710809).
    Thanks to Simon Ruderich for providing patch.
  * d/patches/0004-corrupted-worker-activation-status.patch:
    Fix "Worker activation state corrupted when using jkmanager",
    Thanks to David Gubler for patch (Closes: #711934).

 -- Damien Raude-Morvan <drazzib@debian.org>  Mon, 12 Aug 2013 10:28:44 +0200

libapache-mod-jk (1:1.2.37-2) unstable; urgency=low

  * Re-enable Apache 2.4 transition after wheezy release (Closes: #666851):
    - d/control: Add Build-Depends apache2-dev and dh-apache2.
    - d/rules: Call apache2 dh addon.
    - d/libapache2-mod-jk.{postinst,postrm}: Replace with
      d/libapache2-mod-jk.apache2.
    - d/control: Remove explicit Depends on apache2.2-common.
  * d/control: Bump Standards-Version to 3.9.4: no changes needed.
  * d/control: Use canonical URL for Vcs-* fields.

 -- Damien Raude-Morvan <drazzib@debian.org>  Sat, 01 Jun 2013 15:14:00 +0200

libapache-mod-jk (1:1.2.37-1) unstable; urgency=low

  * New upstream release.

 -- Damien Raude-Morvan <drazzib@debian.org>  Sun, 03 Jun 2012 23:09:32 +0200

libapache-mod-jk (1:1.2.36-1) unstable; urgency=low

  * New upstream release.
  * Revert Apache 2.4 transition (ie. just for wheezy release).
  * Refresh patches.

 -- Damien Raude-Morvan <drazzib@debian.org>  Fri, 18 May 2012 19:20:50 +0200

libapache-mod-jk (1:1.2.35-1) experimental; urgency=low

  * New upstream release:
    - d/patches/0004-compiler-hardening.patch: Merged upstream.
  * d/rules: Just use dh_auto. No need to force using sub-directory as
    debhelper is doing it for us.
  * Prepare Apache 2.4 transition (Closes: #666851):
    - d/control: Add Build-Depends apache2-dev and dh-apache2.
    - d/rules: Call apache2 dh addon.
    - d/libapache2-mod-jk.{postinst,postrm}: Replace with
      d/libapache2-mod-jk.apache2.
    - d/control: Remove explicit Depends on apache2.2-common.
  * d/control: Bump Standards-Version to 3.9.3, no changes needed.
  * d/copyright: Upgrade to copyright-format 1.0.

 -- Damien Raude-Morvan <drazzib@debian.org>  Wed, 04 Apr 2012 22:32:12 +0200

libapache-mod-jk (1:1.2.32-2) unstable; urgency=low

  * Team upload.
  * Set debian/compat to 9; bump debhelper dependency to 8.1.3.
  * Modify debian/rules to enable hardening flags
    and add patches/0004-compiler-hardening.patch (Closes: #656876)
  * Remove Michael Koch from Uploaders. (Closes: #654045)

 -- tony mancill <tmancill@debian.org>  Sat, 04 Feb 2012 07:17:54 +0000

libapache-mod-jk (1:1.2.32-1) unstable; urgency=low

  * New upstream release:
    - Fix whitespace trimming when parsing attribute lists. LP: #592576.
  * Add myself in Uploaders.
  * Include a sensible default configuration in
    /etc/apache2/mods-available/jk.conf
    and remove old sample in /usr/share/doc/libapache2-mod-jk/.
    LP: #118649.
  * Describe changes in upstream handling of JkMount in global scope
    vs in VirtualHost scope (in d/README and default configuration).
    Closes: #460398.
  * Bump Standards-Version to 3.9.2:
    - d/control: Add recommended get-orig-source target.
  * d/watch: Update to new upstream layout.
  * Refresh patches.
  * d/copyright: Upgrade to DEP-5 format.
  * d/README.source: Removed (aka dpatch one)
  * d/libapache-mod-jk.*: Remove old traces from Apache 1.3
    (dropped since lenny).
  * d/rules: Switch to dh7 handling.
  * d/compat: Switch to debhelper compat level 8.
  * Replace d/patches/0004 by autotools_dev dh sequence addons.
  * d/rules: Enable LFS with -D_FILE_OFFSET_BITS=64. Closes: #590075.

 -- Damien Raude-Morvan <drazzib@debian.org>  Thu, 14 Jul 2011 01:15:52 +0200

libapache-mod-jk (1:1.2.31-1) unstable; urgency=low

  * Team upload.
  * Bump debhelper compatibility level to 7.
  * Bump Standards-Version to 3.9.1. No changes were required.
  * Remove duplicated control fields in binary packages.
  * Fix lintian warning about dh_clean -k deprecation.
  * Update package section to httpd.
  * Document in NEWS the minimal Linux version needed (>= 2.6.27) to use
    this module.

 -- Miguel Landaeta <miguel@miguel.cc>  Tue, 15 Feb 2011 09:29:23 -0430

libapache-mod-jk (1:1.2.30-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Convert patches to dep3 format.
  * Switch to source format 3.0.
  * Remove Stefan (Gybas) and Arnaud from Uploaders list. Thanks to your
    contribution in the past!
  * Add Vcs-* headers.
  * Add missing Depends: ${misc:Depends}.
  * Update Standards-Version: 3.9.0 (no changes).
  * Update patch for config.guess and config.sub.
  * Switch to tomcat6 and default-java in workers.properties. Thanks to
    Olivier Berger. (Closes: #590078)

 -- Torsten Werner <twerner@debian.org>  Sat, 24 Jul 2010 01:04:36 +0200

libapache-mod-jk (1:1.2.28-2) unstable; urgency=low

  * Added debian/patches/05_config_update.dpatch which updates
    config.{guess|sub} in native/scripts/build/unix/ (Closes: #540392).
  * debian/control: Let libapache2-mod-jk suggest tomcat6 instead of
    tomcat5.5.
  * Added debian/README.source.
  * Updated Standards-Version to 3.8.3.

 -- Michael Koch <konqueror@gmx.de>  Thu, 20 Aug 2009 20:04:39 +0200

libapache-mod-jk (1:1.2.28-1) unstable; urgency=low

  * New upstream release.
    - Removed debian/patches/05_bug_451494.dpatch. Applied upstream.
    - Removed debian/patches/06_CVE-2008-5519.dpatch. Applied upstream.
  * Updated Build-Depends to debhelper (>= 5) as 4 is deprecated.
  * Link /usr/share/common-licenses/Apache-2.0 in debian/copgyright.
  * Updated Standards-Version to 3.8.2.

 -- Michael Koch <konqueror@gmx.de>  Sat, 25 Jul 2009 23:08:41 +0200

libapache-mod-jk (1:1.2.26-2.1) unstable; urgency=high

  * Non-maintainer upload by the security-team.
  * CVE-2008-5519: Fix information disclosure vulnerability when clients
    abort connection before sending POST body (closes: #523054).

 -- Stefan Fritsch <sf@debian.org>  Sat, 30 May 2009 15:49:20 +0200

libapache-mod-jk (1:1.2.26-2) unstable; urgency=low

  * Apply patch to fix JkOptions handling for virtual hosts. Thanks to
    Toshihiro Sasajima for the patch, Closes: #451494
  * Fixed debian/copyright to mention copyright and license properly.
  * debian/libapache-mod-jk-doc.doc-base: Moved to section
    System/Administration.
  * Remove unused lintian override for libapache-mod-jk-doc.

 -- Michael Koch <konqueror@gmx.de>  Wed, 02 Apr 2008 23:09:41 +0200

libapache-mod-jk (1:1.2.26-1) unstable; urgency=low

  * New upstream release.
  * Updated Standards-Version to 3.7.3.
  * Fixed URL in Homepage field.
  * Fixed typo in debian/libapache2-mod-jk.NEWS.

 -- Michael Koch <konqueror@gmx.de>  Thu, 27 Dec 2007 13:04:55 -0100

libapache-mod-jk (1:1.2.25-2) unstable; urgency=low

  * debian/workers.properties: Renamed worker.loadbalancer.balanced_workers to
    worker.loadbalancer.balance_workers. Closes: #448062.

 -- Michael Koch <konqueror@gmx.de>  Thu, 25 Oct 2007 21:18:33 +0200

libapache-mod-jk (1:1.2.25-1) unstable; urgency=low

  * New upstream release.
  * Don't suggest tomcat5 anymore.
  * Use Homepage: field in debian/control

 -- Michael Koch <konqueror@gmx.de>  Sat, 15 Sep 2007 09:07:30 +0200

libapache-mod-jk (1:1.2.23-4) unstable; urgency=low

  * libapache2-mod-jk: Removed Suggests on tomcat4.
  * libapache-mod-jk-doc: Removed Suggests on libapache-mod-jk.

 -- Michael Koch <konqueror@gmx.de>  Thu,  5 Jul 2007 13:44:24 +0200

libapache-mod-jk (1:1.2.23-3) unstable; urgency=low

  * Don't build libapache-mod-jk binary package anymore. Closes: #429125.
  * Removed Wolfgang from Uploaders.

 -- Michael Koch <konqueror@gmx.de>  Sun, 24 Jun 2007 18:34:33 -0100

libapache-mod-jk (1:1.2.23-2) unstable; urgency=low

  * Point workers.tomcat_home to /usr/share/tomcat5 and
    workers.java_home to /usr/lib/jvm/java-gcj.
  * Generate changelog from changelog.html correctly.

 -- Michael Koch <konqueror@gmx.de>  Sat, 09 Jun 2007 16:06:13 -0100

libapache-mod-jk (1:1.2.23-1) unstable; urgency=high

  * New upstream release.
    - Forward unparsed URI to tomcat. Closes: #425836.
      CVE-2007-1860

 -- Michael Koch <konqueror@gmx.de>  Sat,  2 Jun 2007 23:14:13 +0200

libapache-mod-jk (1:1.2.22-1) unstable; urgency=low

  * New upstream release
    - works when no JkWorkersFile option set (Closes: #419448).
  * Added debian/patches/04_no-worker-error.dpatch to make the message
    "Could not find worker" an error and more visible (Closes: #418887).

 -- Michael Koch <konqueror@gmx.de>  Tue, 17 Apr 2007 08:12:38 +0200

libapache-mod-jk (1:1.2.21-1) unstable; urgency=low

  * New upstream release

 -- Michael Koch <konqueror@gmx.de>  Thu, 12 Apr 2007 07:21:37 +0000

libapache-mod-jk (1:1.2.18-3) unstable; urgency=medium

  * debian/rules: copy source tree for native part to compile seperately for
    Apache 2.0 (Closes: #396224, #406636).
  * Added tomcat5.5 to Depends on tomcat4 | tomcat5 | tomcat5.5.
  * Updated Standards-Version to 3.7.2.
  * Added myself to Uploaders.

 -- Michael Koch <konqueror@gmx.de>  Sat, 17 Feb 2007 17:10:27 +0100

libapache-mod-jk (1:1.2.18-2) unstable; urgency=low

  * debian/control (Depends): updated to apache2.2-common (closes:
    #391651), thanks to Philippe Marzouk.
  * debian/patches/03_jk-runtime-status_default_location.dpatch:
    added. This change the default location of JkShmFile variable (closes:
    #362004), thanks to Adrian Bridgett.

 -- Arnaud Vandyck <avdyk@debian.org>  Mon,  9 Oct 2006 15:41:47 +0200

libapache-mod-jk (1:1.2.18-1) unstable; urgency=low

  * New upstream
  * debian/watch: added
  * Added myself to uploaders
  * debian/rules: modified to revert changes in upstream structure (jk
    directory does not exist anymore)

 -- Arnaud Vandyck <avdyk@debian.org>  Wed,  2 Aug 2006 11:11:03 +0200

libapache-mod-jk (1:1.2.14.1-2) unstable; urgency=low

  * Fixed binary arch only build by splitting
    arch and indep install targets

 -- Wolfgang Baer <WBaer@gmx.de>  Mon, 03 Oct 2005 13:25:14 +0200

libapache-mod-jk (1:1.2.14.1-1) unstable; urgency=low

  * New upstream release (closes: #307331)
  * Removed cdbs dependency - building only with debhelper
  * Build Apache2 package (closes: #296345)
    + debian/control: package libapache2-mod-jk added
    + debian/control: added apache2-threaded-dev build dependency
  * Split documentation in own package so it does not ship twice
    + debian/control: package libapache-mod-jk-doc added
    + debian/control: xsltproc dependency for building
    + debian/control: added conflicts/replaces with old libapache-mod-jk
  * Updated workers.properties
  * Added example httpd.conf files for Apache 1.3 and 2
  * Added tomcat5 as alternative to tomcat4 to suggests
  * Only provide example conf files (for Apache1.3/2) which have to be
    installed and modified for a specific setup by the user (closes: #321203)
  * Added conflicts/replaces with libapache2-mod-jk2 and added NEWS.Debian
  * Registered documentation with doc-base
  * Added lintian override for changelog html file used in the manual
  * Updated copyright to new upstream Apache License Version 2.0
  * Updated Standards-Version to 3.6.2 - no changes

 -- Wolfgang Baer <WBaer@gmx.de>  Tue,  6 Sep 2005 14:43:49 +0200

libapache-mod-jk (1:1.2.5-2) unstable; urgency=low

  * Added libtool build dependency (closes: #229395)

 -- Stefan Gybas <sgybas@debian.org>  Fri, 30 Jan 2004 17:22:12 +0100

libapache-mod-jk (1:1.2.5-1) unstable; urgency=low

  * New upstream release (libapache-mod-jk was previously built by the tomcat
    source package)
  * Updated download location in copyright file
  * Mention in the long description that this module can talk to a remote
    Tomcat server so it can go into main
  * Updated to the new module handling in Apache 1.3.29 and support Apache,
    Apache-Perl and Apache-SSL

 -- Stefan Gybas <sgybas@debian.org>  Sat, 10 Jan 2004 17:18:52 +0100

