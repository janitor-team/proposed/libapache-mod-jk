Source: libapache-mod-jk
Section: httpd
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Markus Koschany <apo@debian.org>
Build-Depends:
 ant,
 apache2-dev,
 debhelper-compat (= 13),
 default-jdk,
 libtool
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/java-team/libapache-mod-jk.git
Vcs-Browser: https://salsa.debian.org/java-team/libapache-mod-jk
Homepage: http://tomcat.apache.org

Package: libapache2-mod-jk
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 libapache-mod-jk-doc,
 tomcat9
Description: Apache 2 connector for the Tomcat Java servlet engine
 Apache Tomcat is the reference implementation for the Java Servlet and
 JavaServer Pages (JSP) specification from the Apache Jakarta project.
 .
 This package contains an Apache 2 module (mod_jk) to forward requests
 from Apache to Tomcat using the AJP 1.3 or 1.4 protocol. It can either
 talk to Tomcat on the local machine or to a remote engine using TCP.

Package: libapache-mod-jk-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends}
Suggests:
 libapache2-mod-jk
Description: Documentation of libapache2-mod-jk package
 Documentation and examples of the Apache jk connector for the Tomcat
 Java servlet engine.
 .
 For uptodate documentation about Tomcat connectors please take a look
 at the home page at http://tomcat.apache.org/connectors-doc/.
